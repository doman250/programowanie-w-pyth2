# from pygame import mouse, MOUSEBUTTONDOWN, display, sprite, QUIT, quit, event, time, Surface, font
import random
import pygame
 
#--- Global constants ---
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
 
SCREEN_WIDTH = 700
SCREEN_HEIGHT = 500
 
# --- Classes ---
 
class Block(pygame.sprite.Sprite):
    """ This class represents a simple block the player collects. """
 
    def __init__(self):
        """ Constructor, create the image of the block. """
        super().__init__()
        self.image = pygame.Surface([20, 20])
        self.image.fill(BLACK)
        self.rect = self.image.get_rect()
 
    def reset_pos(self):
        """ Called when the block is 'collected' or falls off
           the screen. """
        self.rect.y = random.randrange(-300, -20)
        self.rect.x = random.randrange(SCREEN_WIDTH)
 
    def update(self):
        """ Automatically called when we need to move the block. """
        self.rect.y += 1
 
        if self.rect.y > SCREEN_HEIGHT + self.rect.height:
            self.reset_pos()
 
class Player(pygame.sprite.Sprite):
    """ This class represents the player. """
    #CODE HERE: define init class similar to block class above
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([15, 15])
        self.image.fill(RED)
        self.rect = self.image.get_rect()
    #approx 5 lines
 
    def update(self):
        """ Update the player location. """
        pos = pygame.mouse.get_pos()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
 
class Game(object):
    """ This class represents an instance of the game. If we need to
       reset the game we'd just need to create a new instance of this
       class. """
 
    # --- Class attributes.
    # In this case, all the data we need
    # to run our game.
 
    # Sprite lists
    block_list = None
    all_sprites_list = None
    player = None
 
    # Other data
    game_over = False
    score = 0
    leftOut = 0
 
    # --- Class methods
    # Set up the game
    def __init__(self):
        self.score = 0
        #CODE HERE: set self.game_over into False or True
        self.game_over = False
        # Create sprite lists
        self.block_list = pygame.sprite.Group()
        self.all_sprites_list = pygame.sprite.Group()
 
        # Create the block sprites
        for i in range(50):
            block = Block()
 
            #CODE HERE: use random.randrange on SCREEN_WIDTH and assign it to block.rect.x
            block.rect.x = random.randrange(-300, SCREEN_WIDTH)
            block.rect.y = random.randrange(-300, SCREEN_HEIGHT)
 
            self.block_list.add(block)
            self.all_sprites_list.add(block)
 
        # Create the player
        #CODE HERE: assign Player() to self.player
        self.player = Player()
        self.all_sprites_list.add(self.player)
 
    def process_events(self):
        """ Process all of the events. Return a "True" if we need
           to close the window. """
 
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                #CODE HERE: remember to return True or False val
                return True
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.game_over:
                    self.__init__()
 
        return False
 
    def run_logic(self):
        """
       This method is run each time through the frame. It
       updates positions and checks for collisions.
       """
        if not self.game_over:
            # Move all the sprites
            self.all_sprites_list.update()
            # See if the player block has collided with anything.
            blocks_hit_list = pygame.sprite.spritecollide(self.player, self.block_list, True)
            self.leftOut = self.all_sprites_list.__len__() + self.score - 51
            # Check the list of collisions.
            for block in blocks_hit_list:
                self.score += 1
                print(self.score)
                self.player.image = pygame.Surface([30, 30])
                self.player.image.fill(RED)
                self.player.image = pygame.Surface([20, 20])
                self.player.image.fill(RED)
                # You can do something with "block" here.
 
            if len(self.block_list) == 0:
                self.game_over = True
 
    def display_frame(self, screen):
        """ Display everything to the screen for the game. """
        screen.fill(WHITE)
        if self.game_over:
            font = pygame.font.SysFont(None, 25)
            text = font.render("Game Over, click to restart", True, BLACK)
            #CODE HERE: use floor division operator to set desired values in 2 lines below
            center_x = (SCREEN_WIDTH // 2.0) - (text.get_width() // 2.0)
            center_y = (SCREEN_HEIGHT // 2.0) - (text.get_height() // 2.0)
            screen.blit(text, [center_x, center_y])
 
        if not self.game_over:
            self.all_sprites_list.draw(screen)
 
        pygame.display.flip()
 
 
def main():
    pygame.init()
    """ Main program function. """
    # Initialize Pygame and set up the window
    #CODE HERE: call pygame.init()
 
    size = [SCREEN_WIDTH+40, SCREEN_HEIGHT+40]
    screen = pygame.display.set_mode(size)
 
    pygame.display.set_caption("My Game")
    pygame.mouse.set_visible(False)
 
    # Create our objects and set the data
    done = False
    clock = pygame.time.Clock()
 
    # Create an instance of the Game class
    game = Game()
    # Main game loop
    while not done:
 
        # Process events (keystrokes, mouse clicks, etc)
        done = game.process_events()
 
        # Update object positions, check for collisions
        game.run_logic()
 
        # Draw the current frame
        game.display_frame(screen)
 
        # Pause for the next frame
        clock.tick(60)
 
    # Close window and exit
    quit()
 
# Call the main function, start up the game
if __name__ == "__main__":
    main()
