test_string = "Hey! Do you have anything to say? ? ?"

def removeSpaces(str):
    no_space = ""
    for i in range(len(str)):
        if (str[i] !=" "):
         no_space += str[i]
    return no_space

print(removeSpaces(test_string))
