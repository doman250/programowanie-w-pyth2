def sum_seq(sequence):
    try:
        if isinstance(sequence,(list)):
            if len(sequence) == 0:
                return 0
            return sum_seq(sequence[0])+sum_seq(sequence[1:])
        else:
            return sequence
    except:
        return 0
seq = [1,[2,[3,[4]]]]
wynik = sum_seq(seq)
print(wynik)
text_file=open("wynik.txt","w")
text_file.write(str(wynik))
text_file.close()
 
