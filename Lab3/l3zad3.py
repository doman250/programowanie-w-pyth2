def main():

        slowo1=input("Podaj pierwsze słowo: ")
        slowo2=input("Podaj drugie słowo: ")

        if sorted(slowo1.lower()) == sorted(slowo2.lower()):
            print("Słowo ", slowo1, "oraz", slowo2, "są anagramami")
        else:
            print("Słowo ", slowo1, "oraz", slowo2, "nie są anagramami")

main()
